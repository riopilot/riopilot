﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="TestCode" Type="Folder">
			<Item Name="Setup96063d.vi" Type="VI" URL="../TestCode/Setup96063d.vi"/>
			<Item Name="GADAHRSDemo.vi" Type="VI" URL="../TestCode/GADAHRSDemo.vi"/>
		</Item>
		<Item Name="SupportFiles" Type="Folder" URL="../rioClient/rioPilotHumanMachineInterface/SupportFiles">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="rioPilotClient.lvclass" Type="LVClass" URL="../rioClient/rioPilotClient/rioPilotClient.lvclass"/>
		<Item Name="rioPilotHumanMachineInterface.lvclass" Type="LVClass" URL="../rioClient/rioPilotHumanMachineInterface/rioPilotHumanMachineInterface.lvclass"/>
		<Item Name="rioPilotStateMachineRapi.lvclass" Type="LVClass" URL="../rioClient/rioPilotStateMachineRapi/rioPilotStateMachineRapi.lvclass"/>
		<Item Name="ClientMain.vi" Type="VI" URL="../rioClient/ClientMain.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="NI_3D Picture Control.lvlib" Type="Library" URL="/&lt;vilib&gt;/picture/3D Picture Control/NI_3D Picture Control.lvlib"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Analog to Digital.vi"/>
				<Item Name="DWDT Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Analog to Digital.vi"/>
				<Item Name="DTbl Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Analog to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Boolean Array.vi"/>
				<Item Name="DWDT Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Boolean Array.vi"/>
				<Item Name="DTbl Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Boolean Array.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="LVRGBAColorTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRGBAColorTypeDef.ctl"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="XDNodeRunTimeDep.lvlib" Type="Library" URL="/&lt;vilib&gt;/Platform/TimedLoop/XDataNode/XDNodeRunTimeDep.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="PwmFpgaDeviceWriter.lvclass" Type="LVClass" URL="../../labqt/DAQ/PwmFpgaDeviceWriter/PwmFpgaDeviceWriter.lvclass"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../labqt/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../labqt/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../labqt/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../labqt/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../labqt/Execution/Threading/Threading.lvclass"/>
			<Item Name="CRioBufferedDeviceReader.lvclass" Type="LVClass" URL="../../labqt/DAQ/CRioBufferedDeviceReader/CRioBufferedDeviceReader.lvclass"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../labqt/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../labqt/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="GadahrsDeviceReader.lvclass" Type="LVClass" URL="../../labqt/DAQ/GadahrsDeviceReader/GadahrsDeviceReader.lvclass"/>
			<Item Name="MadgwickOrientationFilter.lvlib" Type="Library" URL="../../labqt/Analysis/Algorithm/MadgwickAhrs/MadgwickOrientationFilter.lvlib"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../labqt/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="FunctionCalibration.lvclass" Type="LVClass" URL="../../labqt/Calibration/FunctionCalibration/FunctionCalibration.lvclass"/>
			<Item Name="SubPanelInitiate.vi" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/SubPanelInitiate.vi"/>
			<Item Name="AlarmCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/AlarmCodec/AlarmCodec.lvclass"/>
			<Item Name="MemoryStream.lvclass" Type="LVClass" URL="../../labqt/IOStreams/MemoryStream/MemoryStream.lvclass"/>
			<Item Name="Stream.lvclass" Type="LVClass" URL="../../labqt/IOStreams/Stream/Stream.lvclass"/>
			<Item Name="MemoryReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/MemoryReader/MemoryReader.lvclass"/>
			<Item Name="MemoryWriter.lvclass" Type="LVClass" URL="../../labqt/IOStreams/MemoryWriter/MemoryWriter.lvclass"/>
			<Item Name="Codec.lvclass" Type="LVClass" URL="../../labqt/Codecs/Codec/Codec.lvclass"/>
			<Item Name="RpcClient.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RpcClient/RpcClient.lvclass"/>
			<Item Name="SocketStream.lvclass" Type="LVClass" URL="../../labqt/IOStreams/SocketStream/SocketStream.lvclass"/>
			<Item Name="SocketReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/SocketReader/SocketReader.lvclass"/>
			<Item Name="TCP_NODELAY.vi" Type="VI" URL="../../labqt/IOStreams/SupportVIs/TCP_NODELAY.vi"/>
			<Item Name="wsock32.dll" Type="Document" URL="wsock32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SocketWriter.lvclass" Type="LVClass" URL="../../labqt/IOStreams/SocketWriter/SocketWriter.lvclass"/>
			<Item Name="Alarm.lvclass" Type="LVClass" URL="../../labqt/Alarm/Alarm/Alarm.lvclass"/>
			<Item Name="HumanMachineInterface.lvclass" Type="LVClass" URL="../../labqt/HumanMachineInterface/HumanMachineInterface/HumanMachineInterface.lvclass"/>
			<Item Name="FeedbackControllerRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/FeedbackControllerRapi/FeedbackControllerRapi.lvclass"/>
			<Item Name="FeedbackControlCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/FeedbackControlCodec/FeedbackControlCodec.lvclass"/>
			<Item Name="Regulator.lvclass" Type="LVClass" URL="../../labqt/ControlSystem/Regulator/Regulator.lvclass"/>
			<Item Name="DaqRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/DaqRapi/DaqRapi.lvclass"/>
			<Item Name="DaqCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/DaqCodec/DaqCodec.lvclass"/>
			<Item Name="LogRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/LogRapi/LogRapi.lvclass"/>
			<Item Name="LoggerCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/LoggerCodec/LoggerCodec.lvclass"/>
			<Item Name="Logger.lvclass" Type="LVClass" URL="../../labqt/Logging/Logger/Logger.lvclass"/>
			<Item Name="LogWriter.lvclass" Type="LVClass" URL="../../labqt/Logging/LogWriter/LogWriter.lvclass"/>
			<Item Name="StateMachineCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/StateMachineCodec/StateMachineCodec.lvclass"/>
			<Item Name="rioPilotState.lvclass" Type="LVClass" URL="../rioTarget/States/rioPilotState/rioPilotState.lvclass"/>
			<Item Name="State.lvclass" Type="LVClass" URL="../../labqt/StateMachine/States/State/State.lvclass"/>
			<Item Name="StateMachine.lvclass" Type="LVClass" URL="../../labqt/StateMachine/StateMachine/StateMachine.lvclass"/>
			<Item Name="StateMachineEvent.lvclass" Type="LVClass" URL="../../labqt/StateMachine/StateMachineEvent/StateMachineEvent.lvclass"/>
			<Item Name="EventHandler.lvclass" Type="LVClass" URL="../../labqt/Events/EventHandler/EventHandler.lvclass"/>
			<Item Name="Event.lvclass" Type="LVClass" URL="../../labqt/Events/Event/Event.lvclass"/>
			<Item Name="rioPilotStateMachine.lvclass" Type="LVClass" URL="../rioTarget/rioStateMachine/rioPilotStateMachine.lvclass"/>
			<Item Name="FileStream.lvclass" Type="LVClass" URL="../../labqt/IOStreams/FileStream/FileStream.lvclass"/>
			<Item Name="FileWriter.lvclass" Type="LVClass" URL="../../labqt/IOStreams/FileWriter/FileWriter.lvclass"/>
			<Item Name="FileReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/FileReader/FileReader.lvclass"/>
			<Item Name="FeedbackController.lvclass" Type="LVClass" URL="../../labqt/ControlSystem/FeedbackController/FeedbackController.lvclass"/>
			<Item Name="Controller.lvclass" Type="LVClass" URL="../../labqt/ControlSystem/Controller/Controller.lvclass"/>
			<Item Name="rioPilot.lvclass" Type="LVClass" URL="../rioTarget/rioPilot/rioPilot.lvclass"/>
			<Item Name="rioPilotStateMachineRpcServer.lvclass" Type="LVClass" URL="../rioTarget/rioPilotStateMachineRpcServer/rioPilotStateMachineRpcServer.lvclass"/>
			<Item Name="RpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RpcServer/RpcServer.lvclass"/>
			<Item Name="TCPServer.lvclass" Type="LVClass" URL="../../labqt/Networking/TCPServer/TCPServer.lvclass"/>
			<Item Name="FeedbackControlRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/FeedbackControlRpcServer/FeedbackControlRpcServer.lvclass"/>
			<Item Name="ControlSystemRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/ControlSystemRpcServer/ControlSystemRpcServer.lvclass"/>
			<Item Name="SequenceHandler.lvclass" Type="LVClass" URL="../../labqt/Sequence/SequenceHandler/SequenceHandler.lvclass"/>
			<Item Name="SequenceInputRunner.lvclass" Type="LVClass" URL="../../labqt/Sequence/SequenceInputRunner/SequenceInputRunner.lvclass"/>
			<Item Name="SequenceRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/SequenceRpcServer/SequenceRpcServer.lvclass"/>
			<Item Name="SequenceCodec.lvclass" Type="LVClass" URL="../../labqt/Codecs/SequenceCodec/SequenceCodec.lvclass"/>
			<Item Name="AlarmHandler.lvclass" Type="LVClass" URL="../../labqt/Alarm/AlarmHandler/AlarmHandler.lvclass"/>
			<Item Name="AlarmEvent.lvclass" Type="LVClass" URL="../../labqt/Alarm/AlarmEvent/AlarmEvent.lvclass"/>
			<Item Name="AlarmRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/AlarmRpcServer/AlarmRpcServer.lvclass"/>
			<Item Name="FeedbackControllerBufferedDeviceReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/FeedbackControllerBufferedDeviceReader/FeedbackControllerBufferedDeviceReader.lvclass"/>
			<Item Name="InputStreamRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/InputStreamRpcServer/InputStreamRpcServer.lvclass"/>
			<Item Name="LoggerRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/LoggerRpcServer/LoggerRpcServer.lvclass"/>
			<Item Name="OutputBufferedDeviceReader.lvclass" Type="LVClass" URL="../../labqt/IOStreams/OutputBufferedDeviceReader/OutputBufferedDeviceReader.lvclass"/>
			<Item Name="rioPilotEventHandler.lvclass" Type="LVClass" URL="../rioTarget/rioPilotEventHandler/rioPilotEventHandler.lvclass"/>
			<Item Name="AnalogInputEvent.lvclass" Type="LVClass" URL="../../labqt/DAQ/AnalogInputEvent/AnalogInputEvent.lvclass"/>
			<Item Name="DaqRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/DaqRpcServer/DaqRpcServer.lvclass"/>
			<Item Name="FeedbackSequenceRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/FeedbackSequenceRpcServer/FeedbackSequenceRpcServer.lvclass"/>
			<Item Name="FloatingPointSequence.lvclass" Type="LVClass" URL="../../labqt/Sequence/FloatingPointSequence/FloatingPointSequence.lvclass"/>
			<Item Name="DigitalFpgaDeviceWriter.lvclass" Type="LVClass" URL="../../labqt/DAQ/DigitalFpgaDeviceWriter/DigitalFpgaDeviceWriter.lvclass"/>
			<Item Name="SimulatedBufferedDeviceReader.lvclass" Type="LVClass" URL="../../labqt/DAQ/SimulatedBufferedDeviceReader/SimulatedBufferedDeviceReader.lvclass"/>
			<Item Name="SimulatedUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../labqt/DAQ/SimulatedUnbufferedDeviceWriter/SimulatedUnbufferedDeviceWriter.lvclass"/>
			<Item Name="ManualControl.lvclass" Type="LVClass" URL="../rioTarget/States/ManualControl/ManualControl.lvclass"/>
			<Item Name="SequenceRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/SequenceRapi/SequenceRapi.lvclass"/>
			<Item Name="Alarm.xctl" Type="XControl" URL="../../labqt/Alarm/Alarm/XControl/Alarm/Alarm.xctl"/>
			<Item Name="Regulator.xctl" Type="XControl" URL="../../labqt/ControlSystem/Regulator/XControl/Regulator/Regulator.xctl"/>
			<Item Name="RegulatorLister.xctl" Type="XControl" URL="../../labqt/ControlSystem/Regulator/XControl/RegulatorLister/RegulatorLister.xctl"/>
			<Item Name="PrevasPilot.xctl" Type="XControl" URL="../rioClient/rioPilotHumanMachineInterface/Controls/XControl/PrevasPilot.xctl"/>
			<Item Name="Sequence.xctl" Type="XControl" URL="../../labqt/Sequence/SequenceInputRunner/XControl/Sequence.xctl"/>
			<Item Name="SequenceLister.xctl" Type="XControl" URL="../../labqt/Sequence/SequenceHandler/SequenceLister/SequenceLister.xctl"/>
			<Item Name="HtmlFileLogWriter.lvclass" Type="LVClass" URL="../../labqt/Logging/HtmlFileLogWriter/HtmlFileLogWriter.lvclass"/>
			<Item Name="FileLogWriter.lvclass" Type="LVClass" URL="../../labqt/Logging/FileLogWriter/FileLogWriter.lvclass"/>
			<Item Name="ListLogWriter.lvclass" Type="LVClass" URL="../../labqt/Logging/ListLogWriter/ListLogWriter.lvclass"/>
			<Item Name="DisplayLogWriter.lvclass" Type="LVClass" URL="../../labqt/Logging/DisplayLogWriter/DisplayLogWriter.lvclass"/>
			<Item Name="FloatingPointSequenceRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/FloatingpointSequenceRapi/FloatingPointSequenceRapi.lvclass"/>
			<Item Name="InputStreamsToFileOutputStreamHandler.lvclass" Type="LVClass" URL="../../labqt/IOStreams/InputStreamsToFileOutputStreamHandler/InputStreamsToFileOutputStreamHandler.lvclass"/>
			<Item Name="NMEA.lvlib" Type="Library" URL="../../labqt/Codecs/NMEA/NMEA.lvlib"/>
			<Item Name="NormalizeNrOfSamplesToMax.vi" Type="VI" URL="../../labqt/Utilities/Waveform/NormalizeNrOfSamplesToMax.vi"/>
			<Item Name="StateMachineRpcServer.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/Servers/StateMachineRpcServer/StateMachineRpcServer.lvclass"/>
			<Item Name="PidRegulator.xctl" Type="XControl" URL="../../labqt/ControlSystem/Concrete/PIDRegulator/XControl/PidRegulator.xctl"/>
			<Item Name="AlarmRapi.lvclass" Type="LVClass" URL="../../labqt/RemoteProcedureCall/RemoteApis/AlarmRapi/AlarmRapi.lvclass"/>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="LinearCalibration.lvclass" Type="LVClass" URL="../../labqt/Calibration/Concrete/LinearCalibration/LinearCalibration.lvclass"/>
			<Item Name="FeedbackControlSequence.lvclass" Type="LVClass" URL="../../labqt/Sequence/Concrete/FeedbackControlSequence/FeedbackControlSequence.lvclass"/>
			<Item Name="PIDRegulator.lvclass" Type="LVClass" URL="../../labqt/ControlSystem/Concrete/PIDRegulator/PIDRegulator.lvclass"/>
			<Item Name="RegulatorObjectAttributes.ctl" Type="VI" URL="../../labqt/ControlSystem/Regulator/RegulatorObjectAttributes.ctl"/>
			<Item Name="PilotStateObjectAttributes.ctl" Type="VI" URL="../rioTarget/States/rioPilotState/PilotStateObjectAttributes.ctl"/>
			<Item Name="AlarmObjectAttributes.ctl" Type="VI" URL="../../labqt/Alarm/Alarm/AlarmObjectAttributes.ctl"/>
			<Item Name="SequenceInputRunnerObjectAttributes.ctl" Type="VI" URL="../../labqt/Sequence/SequenceInputRunner/SequenceInputRunnerObjectAttributes.ctl"/>
			<Item Name="PidRegulatorObjectAttributes.ctl" Type="VI" URL="../../labqt/ControlSystem/Concrete/PIDRegulator/PidRegulatorObjectAttributes.ctl"/>
			<Item Name="MinMaxAlarm.lvclass" Type="LVClass" URL="../../labqt/Alarm/Concrete/MinMaxAlarm/MinMaxAlarm.lvclass"/>
			<Item Name="MinMaxObjectAttributes.ctl" Type="VI" URL="../../labqt/Alarm/Concrete/MinMaxAlarm/MinMaxObjectAttributes.ctl"/>
			<Item Name="MinMaxAlarm.xctl" Type="XControl" URL="../../labqt/Alarm/Concrete/MinMaxAlarm/XControl/MinMaxAlarm/MinMaxAlarm.xctl"/>
			<Item Name="AlarmInitData.ctl" Type="VI" URL="../../labqt/Alarm/Alarm/XControl/AlarmConfigurator/AlarmInitData.ctl"/>
			<Item Name="InitData.ctl" Type="VI" URL="../../labqt/ControlSystem/Regulator/XControl/RegulatorConfiguration/InitData.ctl"/>
			<Item Name="AlarmLister.xctl" Type="XControl" URL="../../labqt/Alarm/Alarm/XControl/AlarmLister/AlarmLister.xctl"/>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/WaitForOpen.vi"/>
			<Item Name="ChannelIdDelimiter.ctl" Type="VI" URL="../../labqt/Sequence/SequenceInputRunner/ChannelIdDelimiter.ctl"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SetWaveformChannelNames.vi" Type="VI" URL="../../labqt/Utilities/Waveform/SetWaveformChannelNames.vi"/>
			<Item Name="ListFilesWithFileTypes.vi" Type="VI" URL="../../labqt/Utilities/Path/ListFilesWithFileTypes.vi"/>
			<Item Name="SequenceSeriesType.ctl" Type="VI" URL="../../labqt/Sequence/SequenceInputRunner/SequenceSeriesType.ctl"/>
			<Item Name="url_decode.vi" Type="VI" URL="../../labqt/Utilities/String/url_decode.vi"/>
			<Item Name="RegulatorConfiguration.xctl" Type="XControl" URL="../../labqt/ControlSystem/Regulator/XControl/RegulatorConfiguration/RegulatorConfiguration.xctl"/>
			<Item Name="GetConcreteClasses.vi" Type="VI" URL="../../labqt/GUI/SubPanelUtilities/GetConcreteClasses.vi"/>
			<Item Name="AlarmConfiguration.xctl" Type="XControl" URL="../../labqt/Alarm/Alarm/XControl/AlarmConfigurator/AlarmConfiguration.xctl"/>
			<Item Name="GetRawSocketFromConnectionID.vi" Type="VI" URL="../../labqt/IOStreams/SupportVIs/GetRawSocketFromConnectionID.vi"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LeadLag.vi" Type="VI" URL="../../labqt/ControlSystem/Concrete/PIDRegulator/LeadLag.vi"/>
			<Item Name="CompressToMean.vi" Type="VI" URL="../../labqt/Utilities/Waveform/CompressToMean.vi"/>
			<Item Name="sbRIO-9606_RIOimu.lvbitx" Type="Document" URL="../Binaries/Bitfiles/sbRIO-9606_RIOimu.lvbitx"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../labqt/IOStreams/IO/IO.lvclass"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="../../../../Program Files/National Instruments/LabVIEW 2011/resource/lvanlys.dll"/>
			<Item Name="visarc" Type="Document" URL="../../../../Program Files/National Instruments/LabVIEW 2011/resource/visarc"/>
			<Item Name="ni.var.rc" Type="Document" URL="../../../../Program Files/National Instruments/LabVIEW 2011/resource/objmgr/ni.var.rc"/>
			<Item Name="niLvFpgaFormatErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niLvFpgaFormatErrorSource.vi"/>
			<Item Name="niLvFpgaWhatHappensToTopLevelVI.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niLvFpgaWhatHappensToTopLevelVI.ctl"/>
			<Item Name="niFpgaNodeNameForErrorReporting.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/interface/common/niFpgaNodeNameForErrorReporting.ctl"/>
			<Item Name="niLvFpgaAdjustHostInterfaceError.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niLvFpgaAdjustHostInterfaceError.vi"/>
			<Item Name="niFpgaSimulationCallBeginRW.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/interface/Simulation/niFpgaSimulationCallBeginRW.vi"/>
			<Item Name="niLvFpga_StartFifo_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_StartFifo_Dynamic.vi"/>
			<Item Name="niLvFpga_ConfigureFifo_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_ConfigureFifo_Dynamic.vi"/>
			<Item Name="niLvFpga_ReadFifo_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_ReadFifo_Dynamic.vi"/>
			<Item Name="niLvFpgaErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niLvFpgaErrorClusterFromErrorCode.vi"/>
			<Item Name="niLvFpgaMergeErrorWithErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/errors/niLvFpgaMergeErrorWithErrorCode.vi"/>
			<Item Name="niLvFpga_AcknowledgeIrq_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_AcknowledgeIrq_Dynamic.vi"/>
			<Item Name="niLvFpga_WaitOnIrq_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_WaitOnIrq_Dynamic.vi"/>
			<Item Name="StockFPGA_InlinedAdjustHostInterfaceError.vi" Type="VI" URL="../../../../Program Files/National Instruments/LabVIEW 2011/Targets/NI/FPGA/StockFPGA_IntfPrivate/ScriptTemplates/StockFPGA_InlinedAdjustHostInterfaceError.vi"/>
			<Item Name="nirviErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/RVI Host/nirviSupport.llb/nirviErrorClusterFromErrorCode.vi"/>
			<Item Name="niFpgaHostInterfaceSession.ctl" Type="VI" URL="../../../../Program Files/National Instruments/LabVIEW 2011/Targets/NI/FPGA/StockFPGA_IntfPrivate/ScriptTemplates/niFpgaHostInterfaceSession.ctl"/>
			<Item Name="StockFPGA_PlugInOptionalWaitOnIrq.vi" Type="VI" URL="../../../../Program Files/National Instruments/LabVIEW 2011/Targets/NI/FPGA/StockFPGA_IntfPrivate/ScriptTemplates/StockFPGA_PlugInOptionalWaitOnIrq.vi"/>
			<Item Name="StockFPGA_InlinedWaitFor1OrMoreIRQs.vi" Type="VI" URL="../../../../Program Files/National Instruments/LabVIEW 2011/Targets/NI/FPGA/StockFPGA_IntfPrivate/ScriptTemplates/StockFPGA_InlinedWaitFor1OrMoreIRQs.vi"/>
			<Item Name="StockFPGA_InlinedMethodWaitForSingleIRQ.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/interface/nirviMethod/StockFPGA_InlinedMethodWaitForSingleIRQ.vi"/>
			<Item Name="niLvFpga_Close_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_Close_Dynamic.vi"/>
			<Item Name="niLvFpga_StopFifo_Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/Dynamic/niLvFpga_StopFifo_Dynamic.vi"/>
			<Item Name="niFpgaDynamicAddResources.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/interface/common/dynamic/niFpgaDynamicAddResources.vi"/>
			<Item Name="nirviWhatTheDeviceIsDoing.ctl" Type="VI" URL="/&lt;vilib&gt;/rvi/ClientSDK/nirviWhatTheDeviceIsDoing.ctl"/>
			<Item Name="nirio_resource_hc.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/nirio_resource_hc.ctl"/>
			<Item Name="niLvFpga_Open_sbRIO-9606.vi" Type="VI" URL="/&lt;vilib&gt;/FPGAPlugInAG/sbRIO-9606/niLvFpga_Open_sbRIO-9606.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
