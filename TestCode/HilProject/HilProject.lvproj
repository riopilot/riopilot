﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="RunTakeoffSequence.vi" Type="VI" URL="../RunTakeoffSequence.vi"/>
		<Item Name="StartVS.vi" Type="VI" URL="../StartVS.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_VS Project ExecutionAPI.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI Veristand/Execution/Project/NI_VS Project ExecutionAPI.lvlib"/>
				<Item Name="NI_VS Workspace ExecutionAPI.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI Veristand/Execution/Workspace/NI_VS Workspace ExecutionAPI.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Extract Window Names.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/Winevent.llb/Extract Window Names.vi"/>
			<Item Name="Get Window RefNum.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/WINUTIL.LLB/Get Window RefNum.vi"/>
			<Item Name="LVWUtil32.dll" Type="Document" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/LVWUtil32.dll"/>
			<Item Name="NationalInstruments.VeriStand" Type="Document" URL="NationalInstruments.VeriStand">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NationalInstruments.VeriStand.ClientAPI" Type="Document" URL="NationalInstruments.VeriStand.ClientAPI">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NationalInstruments.VeriStand.RealTimeSequenceDefinitionApi" Type="Document" URL="NationalInstruments.VeriStand.RealTimeSequenceDefinitionApi">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Not a Window Refnum" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/WINUTIL.LLB/Not a Window Refnum"/>
			<Item Name="PostMessage Master.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/WINUTIL.LLB/PostMessage Master.vi"/>
			<Item Name="Quit Application.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/Winevent.llb/Quit Application.vi"/>
			<Item Name="Run Application.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/Winevent.llb/Run Application.vi"/>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Window Refnum" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/WINUTIL.LLB/Window Refnum"/>
			<Item Name="WinUtil Master.vi" Type="VI" URL="../../../../labqt/Utilities/Windows/labview_win_util32_8.6/Winevent.llb/WinUtil Master.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
